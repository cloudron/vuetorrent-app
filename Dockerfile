FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN apt-get update && \
    apt install -y --no-install-recommends qbittorrent-nox && \
    apt-get clean && \
    rm -rf /var/cache/apt /var/lib/apt/lists

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# renovate: datasource=github-releases depName=VueTorrent/VueTorrent versioning=semver extractVersion=^v(?<version>.+)$
ARG VUETORRENT_VERSION=2.23.0

RUN wget https://github.com/WDaan/VueTorrent/releases/download/v${VUETORRENT_VERSION}/vuetorrent.zip -O /tmp/vuetorrent.zip && \
    unzip /tmp/vuetorrent.zip -d /tmp && \
    cp -R /tmp/vuetorrent/. /app/code/ && \
    rm -rf /tmp/vuetorrent*

COPY start.sh qBittorrent.conf /app/pkg/

CMD ["/app/pkg/start.sh"]
