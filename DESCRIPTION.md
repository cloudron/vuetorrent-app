## About

qBittorrent is an advanced and multi-platform BitTorrent client with a nice Qt user interface as well as a Web UI for remote control and an integrated search engine. qBittorrent aims to meet the needs of most users while using as little CPU and memory as possible.

Vuetorrent is the sleekest looking WebUI for qBittorrent

## Features

* Polished µTorrent-like User Interface
* No Ads
* Well-integrated and extensible Search Engine
* RSS feed support with advanced download filters (incl. regex)
* Remote control through Web user interface, written with AJAX
* Sequential downloading (Download in order)
* Advanced control over torrents, trackers and peers
* Bandwidth scheduler
* Torrent creation tool
* IP Filtering (eMule & PeerGuardian format compatible)
* IPv6 compliant
* UPnP / NAT-PMP port forwarding support
* Available on all platforms: Windows, Linux, macOS, FreeBSD, OS/2
* Available in ~70 languages

